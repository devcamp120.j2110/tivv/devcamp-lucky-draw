import { Component } from "react";
import '../App.css'

class LuckyDraw extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ball1:25,
            ball2: 27,
            ball3: 35,
            ball4: 15,
            ball5: 29,
            ball6: 26 
        }
    }
    createRandomNumber = () => Math.floor(Math.random() * 99) + 1;
    onBtnGenClick = () =>
    {
        
        
        this.setState({
            ball1:this.createRandomNumber(),
            ball2: this.createRandomNumber(),
            ball3: this.createRandomNumber(),
            ball4: this.createRandomNumber(),
            ball5: this.createRandomNumber(),
            ball6: this.createRandomNumber()
        })
    }
    render() {
        return(
            <div className="container text-center pt-5">
                <p className="text-bold h1">Lucky Draw</p>
                <div className="align row my-3">
                    <div className="bull mx-1">{this.state.ball1}</div>
                    <div className="bull mx-1">{this.state.ball2}</div>
                    <div className="bull mx-1">{this.state.ball3}</div>
                    <div className="bull mx-1">{this.state.ball4}</div>
                    <div className="bull mx-1">{this.state.ball5}</div>
                    <div className="bull mx-1">{this.state.ball6}</div>
                </div>
                <div><button className="btn btn-info" onClick={this.onBtnGenClick}>Generate</button></div>
            </div>

        )
    }
}
export default LuckyDraw;